# SPDX-License-Identifier: GPL-3.0-or-later

bl_info = {
    "name": "Incremental Autosave",
    "author": "Demeter Dzadik",
    "version": (1, 1, 0),
    "blender": (2, 90, 0),
    "location": "blender",
    "description": "Autosaves in a way where subsequent autosaves don't overwrite previous ones",
    "category": "System",
}

import os, tempfile, json
from datetime import datetime
from pathlib import Path
import bpy
from bpy.props import BoolProperty, IntProperty, StringProperty, CollectionProperty
from bpy.types import PropertyGroup, AddonPreferences, UIList
from bpy.app.handlers import persistent
from bl_ui.generic_ui_list import draw_ui_list

# Timestamp format for prefixing autosave file names.
TIME_FMT_STR = '%Y-%M-%d_%H-%M'

# Timestamp of when Blender is launched. Used to avoid creating an autosave when opening Blender.
LAUNCH_TIME = datetime.now()


class INCSAVE_UL_file_paths(UIList):
    def draw_item(
        self, context, layout, data, item, icon_value, active_data, active_propname
    ):
        filepath = item

        row = layout.row()
        split = row.split(factor=0.2)
        split.prop(item, 'name', text="")
        row = split.row()
        if not os.path.exists(item.path):
            row.alert = True
        row.prop(item, 'path', text="")


def get_addon_prefs(context=None):
    context = context or bpy.context
    return context.preferences.addons[__name__].preferences

def update_prefs_on_file(self=None, context=None):
    prefs = get_addon_prefs(context)
    if not type(prefs).loading:
        prefs.save_prefs_to_file()


class PrefsFileSaveLoadMixin:
    """Mix-in class that can be used by any add-on to store their preferences in a file,
    so that they don't get lost when the add-on is disabled.
    To use it, copy this class and the two functions above it, and do this in your code:

    ```
    import bpy, json
    from pathlib import Path

    class MyAddonPrefs(PrefsFileSaveLoadMixin, bpy.types.AddonPreferences):
        some_prop: bpy.props.IntProperty(update=update_prefs_on_file)

    def register():
        bpy.utils.register_class(MyAddonPrefs)
        MyAddonPrefs.register_autoload_from_file()
    ```

    """

    loading = False

    @staticmethod
    def register_autoload_from_file(delay=0.1):
        def timer_func(_scene=None):
            prefs = get_addon_prefs()
            prefs.load_prefs_from_file()
        bpy.app.timers.register(timer_func, first_interval=delay)

    def prefs_to_dict_recursive(self, propgroup: 'IDPropertyGroup') -> dict:
        """Recursively convert AddonPreferences to a dictionary.
        Note that AddonPreferences don't support PointerProperties,
        so this function doesn't either."""
        from rna_prop_ui import IDPropertyGroup
        ret = {}
        
        if hasattr(propgroup, 'bl_rna'):
            rna_class = propgroup.bl_rna
        else:
            property_group_class_name = type(propgroup).__name__
            rna_class = bpy.types.PropertyGroup.bl_rna_get_subclass_py(property_group_class_name)

        for key, value in propgroup.items():
            if type(value) == list:
                ret[key] = [self.prefs_to_dict_recursive(elem) for elem in value]
            elif type(value) == IDPropertyGroup:
                ret[key] = self.prefs_to_dict_recursive(value)
            else:
                if (
                    rna_class and 
                    key in rna_class.properties and 
                    hasattr(rna_class.properties[key], 'enum_items')
                ):
                    # Save enum values as string, not int.
                    ret[key] = rna_class.properties[key].enum_items[value].identifier
                else:
                    ret[key] = value
        return ret

    def apply_prefs_from_dict_recursive(self, propgroup, data):
        for key, value in data.items():
            if not hasattr(propgroup, key):
                # Property got removed or renamed in the implementation.
                continue
            if type(value) == list:
                for elem in value:
                    collprop = getattr(propgroup, key)
                    entry = collprop.get(elem['name'])
                    if not entry:
                        entry = collprop.add()
                    self.apply_prefs_from_dict_recursive(entry, elem)
            elif type(value) == dict:
                self.apply_prefs_from_dict_recursive(getattr(propgroup, key), value)
            else:
                setattr(propgroup, key, value)

    @staticmethod
    def get_prefs_filepath() -> Path:
        addon_name = __package__.split(".")[-1]
        return Path(bpy.utils.user_resource('CONFIG')) / Path(addon_name + ".txt")

    def save_prefs_to_file(self, _context=None):
        data_dict = self.prefs_to_dict_recursive(propgroup=self)

        with open(self.get_prefs_filepath(), "w") as f:
            json.dump(data_dict, f, indent=4)

    def load_prefs_from_file(self):
        filepath = self.get_prefs_filepath()
        if not filepath.exists():
            return

        with open(filepath, "r") as f:
            addon_data = json.load(f)
            type(self).loading = True
            try:
                self.apply_prefs_from_dict_recursive(self, addon_data)
            except Exception as exc:
                # If we get an error raised here, and it isn't handled,
                # the add-on seems to break.
                print(f"Failed to load {__package__} preferences from file.")
                # raise exc
            type(self).loading = False


class AutoSavePath(PropertyGroup):
    name: StringProperty(
        update=update_prefs_on_file
    )
    path: StringProperty(
        name="Autosave Path",
        description="An autosave path. If this filepath doesn't exist, the next existing one will be used. If none are valid, the Blender auto-save path will be used. If that's not valid either, the OS default temp folder will be used",
        subtype='FILE_PATH',
        default="",
        update=update_prefs_on_file
    )


class IncrementalAutoSavePreferences(PrefsFileSaveLoadMixin, AddonPreferences):
    bl_idname = __name__

    save_before_close: BoolProperty(
        name='Save Before File Open',
        description='Save the current file before opening another file',
        default=True,
        update=update_prefs_on_file,
    )
    save_interval: IntProperty(
        name='Save Interval (Minutes)',
        description="Number of minutes between each save while the add-on is enabled",
        default=5,
        min=1,
        max=120,
        soft_max=30,
        update=update_prefs_on_file,
    )

    max_save_files: bpy.props.IntProperty(
        name='Max Backups Per File',
        description='Maximum number of backups to save for each file, 0 means unlimited. Otherwise, the oldest file will be deleted after reaching the limit',
        default=10,
        min=0,
        max=100,
        update=update_prefs_on_file,
    )
    compress_files: bpy.props.BoolProperty(
        name='Compress Files',
        description='Save backups with compression enabled',
        default=True,
        update=update_prefs_on_file,
    )

    autosave_paths: CollectionProperty(type=AutoSavePath)
    active_index: IntProperty()

    def get_valid_autosave_path(self, context):
        """Return an autosave path that will always actually exist, no matter how desperate."""
        # Try the native autosave path first.
        default_path = bpy.context.preferences.filepaths.temporary_directory
        if os.path.exists(default_path):
            return default_path

        # If that doesn't exist, try the other paths specified by the user in the add-on.
        for path in self.autosave_paths:
            if os.path.exists(path.path):
                return path.path

        # If none of those exist, return the .blend's directory.
        if bpy.data.filepath:
            return os.path.dirname(bpy.data.filepath)

        # And if that doesn't exist either, fall back to the sys temp dir.
        sys_temp = tempfile.gettempdir()
        return sys_temp

    def draw(self, context):
        layout = self.layout.column()
        layout.use_property_decorate = False
        layout.use_property_split = True

        split = layout.split(factor=0.4)
        split.row()
        split.label(text="First valid path will be used:")

        native_row = layout.row()
        if not os.path.exists(context.preferences.filepaths.temporary_directory):
            native_row.alert = True
        native_row.prop(
            context.preferences.filepaths,
            'temporary_directory',
            text="Native Autosave Path",
        )

        draw_ui_list(
            layout,
            context,
            class_name='INCSAVE_UL_file_paths',
            list_path=f'preferences.addons["{__package__}"].preferences.autosave_paths',
            active_index_path=f'preferences.addons["{__package__}"].preferences.active_index',
            insertion_operators=True,
            move_operators=True,
            unique_id='Incremental Autosave Path List',
        )

        layout.separator()

        split = layout.split(factor=0.4)
        split.row()
        split.label(
            text="Current autosave path: " + str(self.get_valid_autosave_path(context))
        )

        layout.separator()

        layout.prop(self, 'save_interval')
        layout.prop(self, 'max_save_files')
        layout.prop(self, 'save_before_close')
        layout.separator()
        layout.prop(self, 'compress_files')


def save_file():
    addon_prefs = get_addon_prefs()

    basename = bpy.data.filepath
    if basename == '':
        basename = 'Unnamed.blend'
    else:
        basename = bpy.path.basename(basename)

    try:
        save_dir = bpy.path.abspath(addon_prefs.get_valid_autosave_path(bpy.context))
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)
    except:
        print("Incremental Autosave: Error creating auto save directory.")
        return

    # Delete old files, to limit the number of saves.
    if addon_prefs.max_save_files > 0:
        try:
            # As we prefix saved blends with a timestamp,
            # `sorted()` puts the oldest prefix at the start of the list.
            # This should be quicker than getting system timestamps for each file.
            otherfiles = sorted(
                [name for name in os.listdir(save_dir) if name.endswith(basename)]
            )
            if len(otherfiles) >= addon_prefs.max_save_files:
                while len(otherfiles) >= addon_prefs.max_save_files:
                    old_file = os.path.join(save_dir, otherfiles[0])
                    os.remove(old_file)
                    otherfiles.pop(0)
        except:
            print("Incremental Autosave: Unable to remove old files.")

    # Save the copy.
    time = datetime.now()
    filename = time.strftime(TIME_FMT_STR) + '_' + basename
    backup_file = os.path.join(save_dir, filename)
    try:
        bpy.ops.wm.save_as_mainfile(
            filepath=backup_file, copy=True, compress=addon_prefs.compress_files
        )
        print("Incremental Autosave: Saved file: ", backup_file)
    except:
        print('Incremental Autosave: Error auto saving file.')


@persistent
def save_before_close(_dummy=None):
    # is_dirty means there are changes that haven't been saved to disk
    if bpy.data.is_dirty and get_addon_prefs().save_before_close:
        save_file()


def create_autosave():
    now = datetime.now()
    delta = now - LAUNCH_TIME
    if delta.seconds < 5:
        return get_addon_prefs().save_interval * 60

    if bpy.data.is_dirty:
        save_file()
    return get_addon_prefs().save_interval * 60


@persistent
def register_autosave_timer(_dummy=None):
    bpy.app.timers.register(create_autosave)


def register():
    bpy.utils.register_class(INCSAVE_UL_file_paths)
    bpy.utils.register_class(AutoSavePath)
    bpy.utils.register_class(IncrementalAutoSavePreferences)
    IncrementalAutoSavePreferences.register_autoload_from_file()
    bpy.app.timers.register(create_autosave)
    bpy.app.handlers.load_pre.append(save_before_close)
    bpy.app.handlers.load_post.append(register_autosave_timer)


def unregister():
    save_before_close()
    update_prefs_on_file()
    bpy.app.handlers.load_pre.remove(save_before_close)
    bpy.app.handlers.load_post.remove(register_autosave_timer)
    bpy.app.timers.unregister(create_autosave)
    bpy.utils.unregister_class(IncrementalAutoSavePreferences)
    bpy.utils.unregister_class(AutoSavePath)
    bpy.utils.unregister_class(INCSAVE_UL_file_paths)
