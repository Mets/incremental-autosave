This add-on makes Blender's autosaves a bit more powerful and reliable.

![Addon Preferences UI](/docs/incremental_autosave.png "Addon Preferences UI")

I specifically created this add-on to be able to share my user preferences across multiple computers. Normally, when you specify an autosave path in your Blender preferences, and that path does not exist, autosaves will simply not work at all. Also, Blender's autosaves overwrite each other, so you only have one backup per file, making shorter autosave intervals risky, since you might want to go back more than a few minutes in your saves.

### Features:
- Incremental autosave: Automatically save in configurable time intervals, without each autosave overwriting the previous one.
- Customizable configuration: Set the maximum number of saves per file and the interval between saves according to your preferences.
- Autosave on file switch: Automatically save the current file when opening another, ensuring your progress is always backed up.
- Autosave for unsaved files: Automatically save files that have not been saved before, giving them the name "Unnamed.blend".
- Support multiple computers: Specify a list of file paths, where the first valid one will be used for saving backups. Useful when you share your user preferences across multiple computers.
- Invalid path fallback: If no valid specified file is found, create backups next to the current .blend, or the OS temp folder.